package com.beny.tp1;

import android.support.v7.app.AppCompatActivity;
import android.content.Intent;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AnimalDetails extends AppCompatActivity {

    private Button saveButton;
    TextView val5;
    String animalNameStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal_details);

        TextView animalName = (TextView) findViewById(R.id.animalName);
        ImageView img = (ImageView) findViewById(R.id.img);
        saveButton = findViewById(R.id.save);


        Intent inte = getIntent();
        animalNameStr = inte.getStringExtra("animalName");
        animalName.setText(animalNameStr);

        String imgStr = AnimalList.getAnimal(animalNameStr).getImgFile();
        int imgStrDrawable = getResources().getIdentifier(imgStr, "drawable", this.getPackageName());

        img.setImageResource(imgStrDrawable);
        TextView val1 = (TextView) findViewById(R.id.val1);
        TextView val2 = (TextView) findViewById(R.id.val2);
        TextView val3 = (TextView) findViewById(R.id.val3);
        TextView val4 = (TextView) findViewById(R.id.val4);
        val5 = (TextView) findViewById(R.id.val5);
        val1.setText(Integer.toString(AnimalList.getAnimal(animalNameStr).getHightestLifespan()));
        val2.setText(Integer.toString(AnimalList.getAnimal(animalNameStr).getGestationPeriod()));
        val3.setText(AnimalList.getAnimal(animalNameStr).getStrBirthWeight());
        val4.setText(AnimalList.getAnimal(animalNameStr).getStrAdultWeight());
        val5.setText(AnimalList.getAnimal(animalNameStr).getConservationStatus());




    }


    public void onClickSave(View view)
    {
            AnimalList.getAnimal(animalNameStr).setConservationStatus(val5.getText().toString());
            Intent goback = new Intent(AnimalDetails.this , MainActivity.class);
            Toast.makeText(getApplicationContext(), "enregistré", Toast.LENGTH_SHORT).show();
            startActivity(goback);

    }
}
