package com.beny.tp1;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MyViewHolder extends RecyclerView.ViewHolder {

    TextView tv ;
    ImageView animalImg;
    public MyViewHolder(@NonNull View itemView) {
        super(itemView);

        tv = (TextView) itemView.findViewById(R.id.nameText);
        animalImg = (ImageView) itemView.findViewById(R.id.animalIcon);
    }
}
