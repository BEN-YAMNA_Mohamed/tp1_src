package com.beny.tp1;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MyViewAdapter extends RecyclerView.Adapter<MyViewHolder> {
    Context c;
    String[] animals ;


    public MyViewAdapter (Context cc , String[] animalss){
        this.c=cc;
        this.animals=animalss;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(c).inflate(R.layout.model, viewGroup,false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int i) {
        final String name = animals[i];
        myViewHolder.tv.setText(name);
        String imgStr = AnimalList.getAnimal(name).getImgFile();
        int imgStrDrawable = c.getResources().getIdentifier(imgStr, "drawable", c.getPackageName());
        myViewHolder.animalImg.setImageResource(imgStrDrawable);

        myViewHolder.tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent inte = new Intent(c,AnimalDetails.class);
                inte.putExtra("animalName", name);
                c.startActivity(inte);
            }
        });
    }


    @Override
    public int getItemCount() {
        return animals.length;
    }

}
